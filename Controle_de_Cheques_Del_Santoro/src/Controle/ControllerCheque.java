/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;
import Model.Cheque;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;

/**
 *
 * @author Gabriel
 */
public class ControllerCheque {
    public static int ultimoID = 0;
    public static ArrayList<Model.Cheque> Cheques = new ArrayList();
    
    public static ArrayList listaCheques(){
        return Cheques;
    }
    public static String[] chequeToString(int i){
        Cheque cheque = Cheques.get(i);
        String[] dados = new String[12];        
        dados[0] = String.valueOf(cheque.getId());
        dados[1] = String.valueOf(cheque.getIdCliente());
        dados[2] = cheque.getNomeCliente();
        dados[3] = Model.Cheque.dataToString(cheque.getEmissao());
        dados[4] = cheque.getCpf();
        dados[5] = cheque.getBanco();
        dados[6] = cheque.getAgencia();
        dados[7] = cheque.getContaCorrente();
        dados[8] = cheque.getNumeroCheque();          
        dados[9] = String.valueOf(cheque.getValor());
        dados[10] = Model.Cheque.dataToString(cheque.getVencimento());
        dados[11] = cheque.getCompensado();
        return dados;
    }     
    public static void getUltimoID() throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Cheques.txt");
        FileInputStream in = new FileInputStream(arquivo);
        InputStreamReader conversor = new InputStreamReader(in);
        try (BufferedReader bf = new BufferedReader(conversor)) {
            boolean continua = true;
            String linha;
            while(continua){
                linha = bf.readLine();
                String[] chequelido = null;
                if(linha == null)
                    continua = false;
                else{
                    chequelido = linha.split(";");                    
                    ultimoID = Integer.parseInt(chequelido[0]);
                }
            }
        }
    }    
    public static void criarDiretorioETexto() throws IOException{
        File diretorio = new File("Dados");
        diretorio.mkdir();
        File arquivo = new File(diretorio, "Cheques.txt");
        arquivo.createNewFile();
    }    
        public static void abreLeArquivo() throws IOException, ParseException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Cheques.txt");
        FileInputStream in = new FileInputStream(arquivo);
        InputStreamReader conversor = new InputStreamReader(in);
        Cheques.removeAll(Cheques);
        try (BufferedReader bf = new BufferedReader(conversor)) {
            boolean continua = true;
            String linha;
            while(continua){
                linha = bf.readLine();
                String[] chequelido = null;
                if(linha == null)
                    continua = false;
                else{
                    chequelido = linha.split(";", -1);
                    Cheques.add(new Model.Cheque(
                            Integer.parseInt(chequelido[0]), Integer.parseInt(chequelido[1]), 
                            chequelido[2], Model.Cheque.StringToData(chequelido[3]), chequelido[4], 
                            chequelido[5], chequelido[6], chequelido[7], 
                            chequelido[8], Double.parseDouble(chequelido[9]), Model.Cheque.StringToData(chequelido[10]), 
                            chequelido[11]));
                }
            }
            
        }
	in.close();
    }
    public static void AdicionaCheque(Model.Cheque cheque) throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Cheques.txt");
        FileWriter escrita = new FileWriter(arquivo, true);
        escrita.write(cheque.getId() + ";" + cheque.getIdCliente() + ";" + 
                cheque.getNomeCliente()+ ";" + Model.Cheque.dataToString(cheque.getEmissao()) + ";" + cheque.getCpf()+ ";" +
                cheque.getBanco()+ ";" + cheque.getAgencia()+ ";" + 
                cheque.getContaCorrente()+ ";" + cheque.getNumeroCheque()+ ";" +
                cheque.getValor() + ";" + Model.Cheque.dataToString(cheque.getVencimento()) + ";" + 
                cheque.getCompensado() + ";" + System.lineSeparator());
        escrita.close();   
    }    
        public static void AtualizaCheque(int id, Model.Cheque chequeNovo) throws IOException, ParseException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Cheques.txt");
        abreLeArquivo();
        FileWriter escrita = new FileWriter(arquivo);
        Cheques.set(id-1, chequeNovo);
        int i=0;
        for (i=0; i < Cheques.size(); i++){
            escrita.write(Cheques.get(i).getId() + ";" + Cheques.get(i).getIdCliente()+ ";" + 
                Cheques.get(i).getNomeCliente()+ ";" + Model.Cheque.dataToString(Cheques.get(i).getEmissao()) + ";" + 
                Cheques.get(i).getCpf() + ";" + Cheques.get(i).getBanco() + ";" + Cheques.get(i).getAgencia() + ";" + 
                Cheques.get(i).getContaCorrente() + ";" + Cheques.get(i).getNumeroCheque()+ ";" +
                Cheques.get(i).getValor()+ ";" + Model.Cheque.dataToString(Cheques.get(i).getVencimento()) + ";" + 
                Cheques.get(i).getCompensado()+ ";" + System.lineSeparator());
        }
        escrita.close();
    }  
    
    
    
}
