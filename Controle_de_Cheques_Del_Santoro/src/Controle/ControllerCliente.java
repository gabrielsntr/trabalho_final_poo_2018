package Controle;
import Model.Cliente;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ControllerCliente {
    public static int ultimoID = 0;
    public static ArrayList<Model.Cliente> Clientes = new ArrayList();
    
    public static ArrayList listaClientes(){
        return Clientes;
    }
    public static String[] clienteToString(int i){
        Cliente cliente = Clientes.get(i);
        String[] dados = new String[12];        
        dados[0] = String.valueOf(cliente.getId());
        dados[1] = cliente.getNome();
        dados[2] = cliente.getCpf();
        dados[3] = cliente.getRg();
        dados[4] = cliente.getEndereco();
        dados[5] = cliente.getBairro();
        dados[6] = cliente.getCep();
        dados[7] = cliente.getMunicipio();          
        dados[8] = cliente.getEstado();
        dados[9] = cliente.getTelefone();
        dados[10] = cliente.getCelular();
        dados[11] = cliente.getEmail();
        dados[12] = cliente.getTipoPessoa();
        return dados;
    }    
    public static void getUltimoID() throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Clientes.txt");
        FileInputStream in = new FileInputStream(arquivo);
        InputStreamReader conversor = new InputStreamReader(in);
        try (BufferedReader bf = new BufferedReader(conversor)) {
            boolean continua = true;
            String linha;
            while(continua){
                linha = bf.readLine();
                String[] clientelido = null;
                if(linha == null)
                    continua = false;
                else{
                    clientelido = linha.split(";");                    
                    ultimoID = Integer.parseInt(clientelido[0]);
                }
            }
        }
    }
    
    
    public static void criarDiretorioETexto() throws IOException{
        File diretorio = new File("Dados");
        diretorio.mkdir();
        File arquivo = new File(diretorio, "Clientes.txt");
        arquivo.createNewFile();
    }
    
    
    public static void abreLeArquivo() throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Clientes.txt");
        FileInputStream in = new FileInputStream(arquivo);
        InputStreamReader conversor = new InputStreamReader(in);
        Clientes.removeAll(Clientes);
        try (BufferedReader bf = new BufferedReader(conversor)) {
            boolean continua = true;
            String linha;
            while(continua){
                linha = bf.readLine();
                String[] clientelido = null;
                if(linha == null)
                    continua = false;
                else{
                    clientelido = linha.split(";", -1);
                    Clientes.add(new Model.Cliente(
                            Integer.parseInt(clientelido[0]), clientelido[1], 
                            clientelido[2], clientelido[3], clientelido[4], 
                            clientelido[5], clientelido[6], clientelido[7], 
                            clientelido[8], clientelido[9], clientelido[10], 
                            clientelido[11], clientelido[12]));
                }
            }
            
        }
	in.close();
    }
    public static void AdicionaCliente(Model.Cliente cliente) throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Clientes.txt");
        FileWriter escrita = new FileWriter(arquivo, true);
        escrita.write(cliente.getId() + ";" + cliente.getNome() + ";" + 
                cliente.getCpf() + ";" + cliente.getRg() + ";" + cliente.getEndereco() + ";" +
                cliente.getBairro() + ";" + cliente.getCep() + ";" + 
                cliente.getMunicipio() + ";" + cliente.getEstado() + ";" +
                cliente.getTelefone() + ";" + cliente.getCelular() + ";" + 
                cliente.getEmail() + ";" + cliente.getTipoPessoa() + ";" + System.lineSeparator());
        escrita.close();   
    }
    public static void AtualizaCliente(int id, Model.Cliente clienteNovo) throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Clientes.txt");
        abreLeArquivo();
        FileWriter escrita = new FileWriter(arquivo);
        Clientes.set(id-1, clienteNovo);
        int i=0;
        for (i=0; i < Clientes.size(); i++){
            escrita.write(Clientes.get(i).getId() + ";" + Clientes.get(i).getNome() + ";" + 
                Clientes.get(i).getCpf() + ";" + Clientes.get(i).getRg() + ";" + Clientes.get(i).getEndereco() + ";" +
                Clientes.get(i).getBairro() + ";" + Clientes.get(i).getCep() + ";" + 
                Clientes.get(i).getMunicipio() + ";" + Clientes.get(i).getEstado() + ";" +
                Clientes.get(i).getTelefone() + ";" + Clientes.get(i).getCelular() + ";" + 
                Clientes.get(i).getEmail() + ";" + Clientes.get(i).getTipoPessoa() + ";" + System.lineSeparator());
        }
        escrita.close();
    }    
}
