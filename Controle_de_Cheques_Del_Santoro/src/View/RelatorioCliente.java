/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controle.ControllerCheque;
import Controle.ControllerCliente;
import static Controle.ControllerCliente.Clientes;
import java.awt.event.KeyEvent;
import java.io.IOException;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
/**
 *
 * @author Gabriel
 */
public class RelatorioCliente extends javax.swing.JInternalFrame {
   int idSelecionado = 0;
   boolean origemPesquisa = false;
   boolean origemCheque = false;
   
   DefaultTableModel modelo = new DefaultTableModel(
    new Object [][] {
    },
    new String [] {
        "ID", "Nome", "CPF/CNPJ", "RG/IE", "Celular", "Telefone"
    }
) {
    Class[] types = new Class [] {
        java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
    };
    boolean[] canEdit = new boolean [] {
        false, false, false, false, false, false
    };

    public Class getColumnClass(int columnIndex) {
        return types [columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit [columnIndex];
    }
};

    /**
     * Creates new form RelatorioCliente
     * @throws java.io.IOException
     */
    public RelatorioCliente() throws IOException {
        Controle.ControllerCliente.abreLeArquivo();
        int i;
        int x = ControllerCliente.Clientes.size();    
        modelo.setRowCount(x);
        for(i=0; i < x; i++){
          modelo.setValueAt(Clientes.get(i).getId(), i, 0);
          modelo.setValueAt(Clientes.get(i).getNome(), i, 1);
          modelo.setValueAt(Clientes.get(i).getCpf(), i, 2);
          modelo.setValueAt(Clientes.get(i).getRg(), i, 3);
          modelo.setValueAt(Clientes.get(i).getCelular(), i, 4);
          modelo.setValueAt(Clientes.get(i).getTelefone(), i, 5); 
        }
        
        initComponents();     
        TableColumn colunaID = tabelaRelatorio.getColumnModel().getColumn(0);
        TableColumn colunaNome = tabelaRelatorio.getColumnModel().getColumn(1);
        TableColumn colunaCpf = tabelaRelatorio.getColumnModel().getColumn(2);
        TableColumn colunaRg = tabelaRelatorio.getColumnModel().getColumn(3);
        TableColumn colunaCelular = tabelaRelatorio.getColumnModel().getColumn(4);        
        TableColumn colunaTelefone = tabelaRelatorio.getColumnModel().getColumn(5);
       
        colunaID.setPreferredWidth(5);
        colunaNome.setPreferredWidth(130);
        colunaCpf.setPreferredWidth(50);
        colunaRg.setPreferredWidth(20);
        colunaTelefone.setPreferredWidth(40);
        colunaCelular.setPreferredWidth(60);        
        
        
        this.getRootPane().registerKeyboardAction(e -> {this.dispose();}, 
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), 
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        
    }
    
    private void Filtrar(String texto, int coluna){
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(modelo);
        tabelaRelatorio.setRowSorter(tr);
        if (coluna == -1){
            tr.setRowFilter(RowFilter.regexFilter("(?i)" + texto));
        }else{
            tr.setRowFilter(RowFilter.regexFilter("(?i)" + texto, coluna));
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaRelatorio = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        tfFiltro = new javax.swing.JTextField();
        comboFiltro = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Relatório de Clientes");

        tabelaRelatorio.setAutoCreateRowSorter(true);
        tabelaRelatorio.setModel(modelo);
        tabelaRelatorio.getTableHeader().setReorderingAllowed(false);
        tabelaRelatorio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaRelatorioMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaRelatorio);

        jLabel2.setText("Valor do filtro:");

        tfFiltro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfFiltroKeyReleased(evt);
            }
        });

        comboFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todos", "ID", "Nome", "CPF/CNPJ", "RG/IE", "Celular", "Telefone" }));
        comboFiltro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboFiltroActionPerformed(evt);
            }
        });

        jLabel3.setText("Filtrar campo:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 681, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 457, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tabelaRelatorioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaRelatorioMouseClicked
        // TODO add your handling code here:
        if(evt.getClickCount() == 2 && !evt.isConsumed() && origemPesquisa == true && origemCheque == false){
            evt.consume();
            idSelecionado = Integer.parseInt(String.valueOf(modelo.getValueAt(tabelaRelatorio.rowAtPoint(evt.getPoint()), 0)));
            this.dispose();
            if (idSelecionado > 0){
                TelaCadastroCliente.tfCodCliente.setText(String.valueOf(idSelecionado));
                TelaCadastroCliente.tfNome.setText(ControllerCliente.Clientes.get(idSelecionado-1).getNome());
                TelaCadastroCliente.tfCPFCNPJ.setText(ControllerCliente.Clientes.get(idSelecionado-1).getCpf());
                TelaCadastroCliente.tfRGIE.setText(ControllerCliente.Clientes.get(idSelecionado-1).getRg());
                TelaCadastroCliente.tfEndereco.setText(ControllerCliente.Clientes.get(idSelecionado-1).getEndereco());
                TelaCadastroCliente.tfBairro.setText(ControllerCliente.Clientes.get(idSelecionado-1).getBairro());
                TelaCadastroCliente.tfCEP.setText(ControllerCliente.Clientes.get(idSelecionado-1).getCep());
                TelaCadastroCliente.tfMunicipio.setText(ControllerCliente.Clientes.get(idSelecionado-1).getMunicipio());
                TelaCadastroCliente.tfEstado.setText(ControllerCliente.Clientes.get(idSelecionado-1).getEstado());
                TelaCadastroCliente.tfTelefone.setText(ControllerCliente.Clientes.get(idSelecionado-1).getTelefone());
                TelaCadastroCliente.tfCelular.setText(ControllerCliente.Clientes.get(idSelecionado-1).getCelular());
                TelaCadastroCliente.tfEmail.setText(ControllerCliente.Clientes.get(idSelecionado-1).getEmail());
                TelaCadastroCliente.comboTipoPessoa.setSelectedItem(ControllerCliente.Clientes.get(idSelecionado-1).getTipoPessoa());
                TelaCadastroCliente.isAtualizaCliente = true;
            }                        
        }
        if(evt.getClickCount() == 2 && !evt.isConsumed() && origemPesquisa == true && origemCheque == true){
            evt.consume();
            idSelecionado = Integer.parseInt(String.valueOf(modelo.getValueAt(tabelaRelatorio.rowAtPoint(evt.getPoint()), 0)));
            this.dispose();
            if (idSelecionado > 0){
                TelaCadastroCheque.tfCodCliente.setText(String.valueOf(idSelecionado));
                TelaCadastroCheque.tfNomeCliente.setText(ControllerCliente.Clientes.get(idSelecionado-1).getNome());
                TelaCadastroCheque.tfCPF.setText(ControllerCliente.Clientes.get(idSelecionado-1).getCpf());
            }            
            
        }        
    }//GEN-LAST:event_tabelaRelatorioMouseClicked

    private void tfFiltroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfFiltroKeyReleased
        // TODO add your handling code here:
        String texto = tfFiltro.getText();
        String campo = comboFiltro.getSelectedItem().toString();
        if ("Todos".equals(campo)){
            Filtrar(texto, -1);
        } 
        if ("ID".equals(campo)){
            Filtrar(texto, 0);
        } 
        if ("Nome".equals(campo)){
            Filtrar(texto, 1);
        } 
        if ("CPF/CNPJ".equals(campo)){
            Filtrar(texto, 2);
        } 
        if ("RG/IE".equals(campo)){
            Filtrar(texto, 3);
        }         
        if ("Celular".equals(campo)){
            Filtrar(texto, 4);
        }        
        if ("Telefone".equals(campo)){
            Filtrar(texto, 5);
        }           
        
    }//GEN-LAST:event_tfFiltroKeyReleased

    private void comboFiltroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboFiltroActionPerformed
    
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboFiltro;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaRelatorio;
    private javax.swing.JTextField tfFiltro;
    // End of variables declaration//GEN-END:variables
}
