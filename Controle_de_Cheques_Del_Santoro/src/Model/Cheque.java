/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Gabriel
 */
public class Cheque {
    public int id, idCliente;
    public String nomeCliente, cpf, banco, agencia, contaCorrente, numeroCheque, compensado;
    public Date emissao, vencimento;
    public double valor;
    
    public Cheque(int id, int idCliente, String nomeCliente, Date emissao, 
            String cpf, String banco, String agencia, String contaCorrente, 
            String numeroCheque, double valor, Date vencimento, 
            String compensado){
        this.id = id;
        this.idCliente = idCliente;
        this.nomeCliente = nomeCliente;
        this.emissao = emissao;
        this.cpf = cpf;
        this.banco = banco;
        this.agencia = agencia;
        this.contaCorrente = contaCorrente;
        this.numeroCheque = numeroCheque;
        this.valor = valor;
        this.vencimento = vencimento;
        this.compensado = compensado;        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(String contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public String getCompensado() {
        return compensado;
    }

    public void setCompensado(String compensado) {
        this.compensado = compensado;
    }

    public Date getEmissao() {
        return emissao;
    }

    public void setEmissao(Date emissao) {
        this.emissao = emissao;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    public static String dataToString(Date data){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = df.format(data);
        return dataFormatada;
    }
    public static Date StringToData(String data) throws ParseException{
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date dataFormatada = df.parse(data);
        return dataFormatada;
    }
    
    
}
